package org.mosi.grease.parser;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.mosi.grease.datamodel.types.*;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class Parser {

    String id;

    List<Version> versions;

    public Parser (String id) {
        this.id = id;
    }

    public void parse() {
            findVersions();
            recognizeEntitiesAndExtractMetainformation();
            deriveActivitiesAndRelationships();
            connectToRelatedStudies();
            exportJSON();
    }

    private void findVersions() {

        Document doc = null;
        try {
            doc = Jsoup.connect("https://www.ebi.ac.uk/biomodels/" + id).get();
        } catch (IOException e) {
            e.printStackTrace();
        }

 /*      //Save HTML
        String docString = doc.toString();
        String filePath = id + ".html";
        try(FileWriter fileWriter = new FileWriter(filePath)){
            fileWriter.write(docString);
        } catch (IOException e) {
            e.printStackTrace();
        }*/

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMM d, yyyy h:mm:ss a").withLocale(Locale.US);

        List<Version> versionList = new ArrayList<>();

        Elements revisions = doc.getElementById("History").select("ul > li:contains(Version)");

        for (Element revision : revisions) {
            String version = revision.ownText().split(" ")[1];
            Elements links = revision.select("a.versionDownload[href]");
            String link = "";

            for (Element linkElement : links) {
                if (linkElement.attr("title").contains("go to version")) {
                    link = "https://www.ebi.ac.uk" + linkElement.attr("href");
                    break;
                }
            }
            if(link.equals("")) {
                link = "https://www.ebi.ac.uk/biomodels/" + id;
            }

            Elements submittedOnElements = revision.select("ul > li:contains(Submitted on)");
            LocalDateTime submittedOn = null;
            if (!submittedOnElements.isEmpty()) {
                String submittedOnText = submittedOnElements.first().ownText().substring(13).trim(); // Remove "Submitted on: "
                submittedOn = LocalDateTime.parse(submittedOnText, formatter);
            }

            Elements submittedByElements = revision.select("ul > li:contains(Submitted by)");
            String submittedBy = "";
            if (!submittedByElements.isEmpty()) {
                submittedBy = submittedByElements.first().ownText().substring(13); // Remove "Submitted by: "
            }

            Version v = new Version(link, version, submittedOn, submittedBy);

            try {
                doc = Jsoup.connect(link).get();
            } catch (IOException e) {
                e.printStackTrace();
            }
            v.setDocument(doc);

            versionList.add(v);
        }

        versionList.sort(Comparator.comparingInt(v -> Integer.parseInt(v.getNumber())));
        versions = versionList;
    }

    private void recognizeEntitiesAndExtractMetainformation() {

        for(Version v : versions) {

            //Simulation Models and Simulation Experiments
            Map<String, String> smFileMap = new HashMap<>();
            Map<String, String> seFileMap = new HashMap<>();
            Map<String, String> seDescriptionMap = new HashMap<>();
            Map<String, String> sdFileMap = new HashMap<>();
            Map<String, String> sdDescriptionMap = new HashMap<>();
            Map<String, String> idFileMap = new HashMap<>();
            Map<String, String> idDescriptionMap = new HashMap<>();
            Map<String, String> qmFileMap = new HashMap<>();
            Elements rows = v.getDocument().select("#Files table tbody tr");
            for (Element row : rows) {
                Elements cols = row.select("td");
                if (cols.size() > 0) {
                    String fileName = cols.get(0).text();

                    if(!fileName.equals("Model files") && !fileName.equals("Additional files")) {
                        String description = cols.get(1).text();

                        if (fileName.endsWith(".xml")) {
                            smFileMap.put(fileName , "SBML");
                        } else if(fileName.endsWith(".cps")) {
                            seFileMap.put(fileName, "COPASI");
                            seDescriptionMap.put(fileName, description);
                        } else if(fileName.endsWith(".sedml")) {
                            seFileMap.put(fileName, "SED-ML");
                            seDescriptionMap.put(fileName, description);
                        } else if(fileName.endsWith(".png") && !description.contains("Reaction graph")) {
                            sdFileMap.put(fileName, "PNG");
                            sdDescriptionMap.put(fileName, description);
                        } else if(fileName.endsWith(".csv")) {
                            idFileMap.put(fileName, "CSV");
                            idDescriptionMap.put(fileName, description);
                        } else if(fileName.endsWith(".svg")) {
                            qmFileMap.put(fileName, "SVG");
                        } else if (fileName.endsWith(".pdf")) {
                            qmFileMap.put(fileName, "PDF");
                        } else if (fileName.endsWith(".png")) {
                            qmFileMap.put(fileName, "PNG");
                        }
                    }
                }
            }

            //current assumption: there is only one entity of each type per version

            SimulationModel sm = new SimulationModel("SM" + v.getNumber());
            sm.setFilesAndFormats(smFileMap);
            sm.setBioModelsIdentifier(id);
            sm.setDescription(extractShortDescription(v.getDocument()));
            sm.setModelingApproach(extractModelingApproach(v.getDocument()));
            sm.setSubmittedOn(v.getSubmittedOn());
            sm.setSubmittedBy(v.getSubmittedBy());
            v.getEntities().add(sm);

            SimulationExperiment se = new SimulationExperiment("SE" + v.getNumber());
            se.setFilesAndFormats(seFileMap);
            se.setFilesAndDescriptions(seDescriptionMap);
            v.getEntities().add(se);

            //Simulation Data
            if(!sdFileMap.isEmpty()) {
                SimulationData sd = new SimulationData("SD" + v.getNumber());
                sd.setFilesAndFormats(sdFileMap);
                sd.setFilesAndDescriptions(sdDescriptionMap);
                v.getEntities().add(sd);
            }

            //Input Data
            if(!idFileMap.isEmpty()) {
                InputData id = new InputData("ID" + v.getNumber());
                id.setFilesAndFormats(idFileMap);
                id.setFilesAndDescriptions(idDescriptionMap);
                v.getEntities().add(id);
            }

            //Conceptual Model (Qualitative Model)
            Map<String, List<String>> metadata = extractMetadata(v.getDocument());
            Map<String, String> parameterMap = extractParameters(v.getDocument());
            Map<String, String> variableMap = extractVariables(v.getDocument());
            if(!(metadata.isEmpty() && qmFileMap.isEmpty() && parameterMap.isEmpty() && variableMap.isEmpty())) {
                QualitativeModel qm = new QualitativeModel("QM" + v.getNumber());
                qm.setFilesAndFormats(qmFileMap);
                qm.setMetadata(metadata);
                qm.setParametersAndValues(parameterMap);
                qm.setVariablesAndInitialValues(variableMap);
                v.getEntities().add(qm);
            }

            //Curation Data
            CurationData cd = extractCurationMetadata(v.getDocument(), v.getNumber());
            if(cd != null) {
                v.getEntities().add(cd);

                //Publication
                String reference = extractPublicationLink(v.getDocument());
                if(!reference.equals("")) {
                    Publication p = new Publication("P" + v.getNumber());
                    p.setReference(reference);
                    v.getEntities().add(p);
                }
            }

        }
    }


    private CurationData extractCurationMetadata(Document document, String version) {

        Element curationDiv = document.getElementById("Curation");
        String comment = curationDiv.select("div[style]").text();
        String dateText = curationDiv.select("em").text();
        String addedDateString = dateText.replaceAll(".*added: ([^,]+, [^,]+),.*", "$1");
        String updatedDateString = dateText.replaceAll(".*updated: ([^,]+, [^,]+)\\).*", "$1");
        DateTimeFormatter formatterCurationDate = DateTimeFormatter.ofPattern("d MMM yyyy, H:mm:ss").withLocale(Locale.US);
        LocalDateTime addedDate = LocalDateTime.parse(addedDateString, formatterCurationDate);
        LocalDateTime updatedDate = LocalDateTime.parse(updatedDateString, formatterCurationDate);

        //check if it belongs to this version
        String curatedVersion = "0";
        for(Version v : versions) {
            if(updatedDate.isAfter(v.getSubmittedOn()) || updatedDate.isEqual(v.getSubmittedOn())) {
                curatedVersion = v.getNumber();
                continue;
            }
            if(updatedDate.isBefore(v.getSubmittedOn())) {
                break;
            }
        }

        if(curatedVersion.equals(version)) {
            CurationData cd = new CurationData("CD" + curatedVersion);
            cd.setDescription(comment);
            cd.setAddedDate(addedDate);
            cd.setUpdatedDate(updatedDate);
            return cd;
        }

        return null;
    }

    private String extractPublicationLink(Document document) {
        Element publicationLink = document.select("a.publicationLink").first();

        if (publicationLink != null) {
            return publicationLink.attr("href");
        }
        return "";
    }

    private Map<String, String> extractVariables(Document document) {
        //not supported anymore
        return new HashMap<>();
    }

    private Map<String, String> extractParameters(Document document) {
        //not supported anymore
        return new HashMap<>();
    }

    private Map<String, List<String>> extractMetadata(Document document) {

        Map<String, List<String>> metadata = new HashMap<>();

        // Extract hasTaxon values
        List<String> hasTaxonValues = new ArrayList<>();
        Elements hasTaxonBlocks = document.select("#hasTaxon1 a");
        for (Element element : hasTaxonBlocks) {
            hasTaxonValues.add(element.text());
        }
        if(!hasTaxonValues.isEmpty()) {
            metadata.put("hasTaxon", hasTaxonValues);
        }

        // Extract hasProperty values
        List<String> hasPropertyValues = new ArrayList<>();
        Elements hasPropertyBlocks = document.select("#hasProperty1 a");
        for (Element element : hasPropertyBlocks) {
            hasPropertyValues.add(element.text());
        }
        if(!hasTaxonValues.isEmpty()) {
            metadata.put("hasProperty", hasPropertyValues);
        }

        // Extract hasPart values
        List<String> hasPartValues = new ArrayList<>();
        Elements hasPartBlocks = document.select("#hasPart1 a");
        for (Element element : hasPartBlocks) {
            hasPartValues.add(element.text());
        }
        if(!hasPartValues.isEmpty()) {
            metadata.put("hasPart", hasPartValues);
        }

        return metadata;
    }

    private String extractModelingApproach(Document document) {
        // Select the div that contains the modeling approach
        Elements approachRows = document.select("#Overview .row");
        String modelingApproach = "";

        for (Element row : approachRows) {
            if (row.text().contains("Modelling approach(es)")) {
                Element approachElement = row.select("a").first();
                if (approachElement != null) {
                    modelingApproach = approachElement.text();
                    break;
                }
            }
        }

        if (!modelingApproach.isEmpty()) {
            return modelingApproach;
        } else {
            return "";
        }
    }

    private String extractShortDescription(Document document) {
        Elements rows = document.select("#Overview .row");

        for (Element row : rows) {
            Elements columns = row.select(".columns");
            if (columns.size() > 1 && columns.get(0).text().equals("Short description")) {
                return columns.get(1).select("#description").text();
            }
        }
        return "";
    }

    private void deriveActivitiesAndRelationships() {

    }

    private void connectToRelatedStudies() {

    }

    private void exportJSON() {

    }
}
