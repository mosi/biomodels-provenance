package org.mosi.grease.parser;

import org.jsoup.nodes.Document;
import org.mosi.grease.datamodel.Entity;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Version {

    private String href;
    private String number;
    private Document doc;
    private LocalDateTime submittedOn;
    private String submittedBy;

    List<Entity> entities;

    public Version(String href, String number, LocalDateTime submittedOn, String submittedBy) {
        this.href = href; this.number = number; this.submittedOn = submittedOn; this.submittedBy = submittedBy;
        this.entities = new ArrayList<>();
    }

    public List<Entity> getEntities() {
        return entities;
    }

    public void setEntities(List<Entity> entities) {
        this.entities = entities;
    }

    public LocalDateTime getSubmittedOn() {
        return submittedOn;
    }

    public void setSubmittedOn(LocalDateTime submittedOn) {
        this.submittedOn = submittedOn;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public void setDocument(Document doc) {
        this.doc = doc;
    }

    public Document getDocument() {
        return doc;
    }

    public String getSubmittedBy() {
        return submittedBy;
    }

    public void setSubmittedBy(String submittedBy) {
        this.submittedBy = submittedBy;
    }
}
