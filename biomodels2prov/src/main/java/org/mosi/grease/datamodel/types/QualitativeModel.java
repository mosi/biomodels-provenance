package org.mosi.grease.datamodel.types;

import java.util.List;
import java.util.Map;

public class QualitativeModel extends ConceptualModel {

    private Map<String, List<String>> metadata;
    private Map<String, String> filesAndFormats;
    private Map<String, String> variablesAndInitialValues;
    private Map<String, String> parametersAndValues;

    public QualitativeModel(String identifier) {
        super("Qualitative Model", identifier);
    }

    public Map<String, List<String>> getMetadata() {
        return metadata;
    }

    public void setMetadata(Map<String, List<String>> metadata) {
        this.metadata = metadata;
    }

    public Map<String, String> getFilesAndFormats() {
        return filesAndFormats;
    }

    public void setFilesAndFormats(Map<String, String> filesAndFormats) {
        this.filesAndFormats = filesAndFormats;
    }

    public Map<String, String> getVariablesAndInitialValues() {
        return variablesAndInitialValues;
    }

    public void setVariablesAndInitialValues(Map<String, String> variablesAndInitialValues) {
        this.variablesAndInitialValues = variablesAndInitialValues;
    }

    public Map<String, String> getParametersAndValues() {
        return parametersAndValues;
    }

    public void setParametersAndValues(Map<String, String> parametersAndValues) {
        this.parametersAndValues = parametersAndValues;
    }

}
