package org.mosi.grease.datamodel.types;

import org.mosi.grease.datamodel.Entity;

public class ConceptualModel extends Entity {
    public ConceptualModel(String type, String identifier) {
        super(type, identifier);
    }
}
