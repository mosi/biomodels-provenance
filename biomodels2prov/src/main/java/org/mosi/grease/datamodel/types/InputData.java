package org.mosi.grease.datamodel.types;

import org.mosi.grease.datamodel.Entity;

import java.util.Map;

public class InputData extends Entity {

    private Map<String, String> filesAndFormats;
    private Map<String, String> filesAndDescriptions;

    public InputData(String identifier) {
        super("Input Data", identifier);
    }

    public Map<String, String> getFilesAndFormats() {
        return filesAndFormats;
    }

    public void setFilesAndFormats(Map<String, String> filesAndFormats) {
        this.filesAndFormats = filesAndFormats;
    }

    public Map<String, String> getFilesAndDescriptions() {
        return filesAndDescriptions;
    }

    public void setFilesAndDescriptions(Map<String, String> filesAndDescriptions) {
        this.filesAndDescriptions = filesAndDescriptions;
    }
}
