package org.mosi.grease.datamodel;

import java.util.List;

public class Entity extends Node {

    private String type;
    private String identifier;

    public Entity(String type, String identifier) {
        super();
        this.type = type;
        this. identifier = identifier;
    }
}
