package org.mosi.grease.datamodel.types;

import org.mosi.grease.datamodel.Entity;

import java.util.Map;

public class SimulationData extends Entity {

    private Map<String, String> filesAndFormats;
    private Map<String, String> filesAndDescriptions;

    public SimulationData(String identifier) {
        super("Simulation Data", identifier);
    }

    public Map<String, String> getFilesAndFormats() {
        return filesAndFormats;
    }

    public void setFilesAndFormats(Map<String, String> filesAndFormats) {
        this.filesAndFormats = filesAndFormats;
    }

    public Map<String, String> getFilesAndDescriptions() {
        return filesAndDescriptions;
    }

    public void setFilesAndDescriptions(Map<String, String> filesAndDescriptions) {
        this.filesAndDescriptions = filesAndDescriptions;
    }
}
