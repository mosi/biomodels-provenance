package org.mosi.grease.datamodel.types;

import org.mosi.grease.datamodel.Entity;

import java.time.LocalDateTime;
import java.util.Map;

public class CurationData extends Entity {

    private String description;
    private String software;
    private LocalDateTime addedDate;
    private LocalDateTime updatedDate;

    public CurationData(String identifier) {
        super("Curation Data", identifier);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSoftware() {
        return software;
    }

    public void setSoftware(String software) {
        this.software = software;
    }

    public LocalDateTime getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(LocalDateTime addedDate) {
        this.addedDate = addedDate;
    }

    public LocalDateTime getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(LocalDateTime updatedDate) {
        this.updatedDate = updatedDate;
    }

}
