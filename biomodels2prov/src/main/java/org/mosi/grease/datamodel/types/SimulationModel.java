package org.mosi.grease.datamodel.types;

import org.mosi.grease.datamodel.Entity;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

public class SimulationModel extends Entity {

    private String biomodelsIdentifier;
    private String description;
    private String modelingApproach;
    private Map<String, String> filesAndFormats;
    private LocalDateTime submittedOn;
    private String submittedBy;

    public SimulationModel(String identifier) {
        super("Simulation Model", identifier);
    }

    public String getBioModelsIdentifier() {
        return biomodelsIdentifier;
    }

    public void setBioModelsIdentifier(String bioModelsIdentifier) {
        this.biomodelsIdentifier = bioModelsIdentifier;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getModelingApproach() {
        return modelingApproach;
    }

    public void setModelingApproach(String modelingApproach) {
        this.modelingApproach = modelingApproach;
    }

    public Map<String, String> getFilesAndFormats() {
        return filesAndFormats;
    }

    public void setFilesAndFormats(Map<String, String> filesAndFormats) {
        this.filesAndFormats = filesAndFormats;
    }

    public LocalDateTime getSubmittedOn() {
        return submittedOn;
    }

    public void setSubmittedOn(LocalDateTime submittedOn) {
        this.submittedOn = submittedOn;
    }

    public String getSubmittedBy() {
        return submittedBy;
    }

    public void setSubmittedBy(String submittedBy) {
        this.submittedBy = submittedBy;
    }
}
