package org.mosi.grease.datamodel.types;

import org.mosi.grease.datamodel.Entity;

public class Publication extends Entity {

    private String reference;

    public Publication(String identifier) {
        super("Publication", identifier);
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }
}
