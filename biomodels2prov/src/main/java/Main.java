import org.mosi.grease.parser.Parser;

public class Main {
    public static void main(String[] args) {

        //Get commandline argument (BiomMdels identifier)
        String id = args[0];
        //Parse the web page
        Parser parser = new Parser(id);
        parser.parse();
    }
}
