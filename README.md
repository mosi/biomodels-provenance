# BioModels Provenance

In this project, we created PROV-DM provenance graphs of selected entries in the BioModels database.
The graphs illustrate the entities and activities involved in the curation process of these models.
In addition to the graphs, we provide supporting documents that describe the entities further with metainformation. 

We include the following simulation studies:


| BioModel ID | Publication |
| ------ | ------ |
| BIOMD0000000591 | Boehm, Martin E., et al. "Identification of isoform-specific dynamics in phosphorylation-dependent STAT5 dimerization by quantitative mass spectrometry and mathematical modeling." Journal of proteome research 13.12 (2014): 5685-5694. |
| BIOMD0000000676 | Chen, Kejing, and Aleksander S. Popel. "Theoretical analysis of biochemical pathways of nitric oxide release from vascular endothelial cells." Free Radical Biology and Medicine 41.4 (2006): 668-680. |
| BIOMD0000000371 | De Vries, Gerda, and Arthur Sherman. "Channel sharing in pancreatic β-cells revisited: enhancement of emergent bursting by noise." Journal of theoretical biology 207.4 (2000): 513-530. |
| BIOMD0000000763 | Dritschel, Heidi, et al. "A mathematical model of cytotoxic and helper T cell interactions in a tumor microenvironment." Letters in Biomathematics 5.S1 (2018). |
| BIOMD0000000955 | Giordano, Giulia, et al. "Modelling the COVID-19 epidemic and implementation of population-wide interventions in Italy." Nature medicine 26.6 (2020): 855-860. |
| BIOMD0000001013 | León-Triana, Odelaisy, et al. "Dual-target CAR-Ts with on-and off-tumour activity may override immune suppression in solid cancers: A mathematical proof of concept." Cancers 13.4 (2021): 703. |
| BIOMD0000000819 | Nazari, Fereshteh, et al. "A mathematical model for IL-6-mediated, stem cell driven tumor growth and targeted treatment." PLoS computational biology 14.1 (2018): e1005920. |
| BIOMD0000000652 | Padala, Rahul Rao, et al. "Cancerous perturbations within the ERK, PI3K/Akt, and Wnt/β-catenin signaling network constitutively activate inter-pathway positive feedback loops." Molecular BioSystems 13.5 (2017): 830-840. |
| BIOMD0000000442 | Sarma, Uddipan, and Indira Ghosh. "Oscillations in MAPK cascade triggered by two distinct designs of coupled positive and negative feedback loops." BMC research notes 5.1 (2012): 1-20. |
| BIOMD0000000437 | Tseng, Yu-Yao, et al. "Comprehensive modelling of the Neurospora circadian clock and its temperature compensation." PLoS computational biology 8.3 (2012): e1002437. |

